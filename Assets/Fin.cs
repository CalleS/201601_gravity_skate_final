﻿using UnityEngine;
using System.Collections;

public class Fin : MonoBehaviour {
    public int damage;

    public LayerMask whattoattack;
    public Transform attackpoint;

    private float time;
    private float timetoattack;
    // Use this for initialization
    void Start () {
        this.time = 0;
        this.timetoattack = 1;
    }
	
	// Update is called once per frame
	void Update () {
        this.time += Time.deltaTime;

        if (this.time > this.timetoattack)
        {
            this.time = 0;
            this.Detect();
        }
    }
    void Detect()
    {
        Collider2D tmp = Physics2D.OverlapCircle(this.attackpoint.position, 0.02f, this.whattoattack);
        if (tmp)
        {
            if (tmp.gameObject.CompareTag("Player"))
            {
                tmp.gameObject.SendMessage("hurt", this.damage);
            }


        }
    }
}
