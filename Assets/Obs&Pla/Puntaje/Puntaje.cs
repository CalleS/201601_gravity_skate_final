﻿using UnityEngine;
using System.Collections;

public class Puntaje : MonoBehaviour {

    public GameObject centenas, decenas, unidades;
    private Animator ce, de, un;

	// Use this for initialization
	void Start () {
        ce = centenas.GetComponent<Animator>();
        de = decenas.GetComponent<Animator>();
        un = unidades.GetComponent<Animator>();
        ce.Play("Estado_09");
        de.Play("Estado_06");
        un.Play("Estado_01");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
