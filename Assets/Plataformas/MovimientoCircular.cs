﻿using UnityEngine;
using System.Collections;

public class MovimientoCircular : MonoBehaviour
{


    public Transform Centro;
    private float xo, yo, x, y, r, angulo, tiempo;

    // Use this for initialization
    void Start()
    {
        r = 3f;
        angulo = Mathf.PI / 4;
        xo = Centro.transform.position.x;
        xo = Centro.transform.position.y;
        tiempo = 0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (tiempo >= 0.1f)
        {
            x = xo + r * Mathf.Cos(angulo);
            y = yo + r * Mathf.Sin(angulo);
            angulo = (angulo - Mathf.PI / 32) % (2 * Mathf.PI);
            transform.localPosition = new Vector2(x, y);
            tiempo = 0f;
        }
        else
        {
            tiempo += Time.deltaTime;
        }
    }

}