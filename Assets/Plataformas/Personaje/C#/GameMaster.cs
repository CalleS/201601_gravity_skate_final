﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour {
    public static GameMaster current;
    public GameObject player;

    public GameObject deathpanel;
    public Text puntdeathtext;

    public Text puntuationtext;

    private Vector3 startPosition;
    private int puntuation;
	// Use this for initialization
	void Start () {
        GameMaster.current = this;
        this.startPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        this.UpdatePuntText();
    }
	
	// Update is called once per frame
    void UpdatePuntText()
    {
        this.puntuationtext.text = "- " + this.puntuation + " -";
    }
    //==========================
    public void AddPuntuation(int amount)
    {
        this.puntuation += amount;
        this.UpdatePuntText();
    }
    public void GameOver()
    {
        this.deathpanel.SetActive(true);
        this.puntdeathtext.text = "Puntuación: " + this.puntuation;

    }
    public void ReloadScene()
    {
        Application.LoadLevel(0);
    }
}
