﻿using UnityEngine;
using System.Collections;

public class Movimiento : MonoBehaviour {

    public GameObject biker;
    public Transform inicio;
    public Transform fin;
    private Transform siguiente;
    public float velocidad;
    private Transform position;

    public LayerMask whattoattack;
    public Transform attackpoint;

    private float time;
    private float timetoattack;

    // Use this for initialization
    void Start () {
        siguiente = fin;

        this.time = 0;
        this.timetoattack = 1;
    }

    // Update is called once per frame
    void Update(){
        this.time += Time.deltaTime;
        if (this.time > this.timetoattack)
        {
            this.time = 0;
            this.attack();
        }
        biker.transform.position = Vector2.MoveTowards(biker.transform.position, siguiente.position, Time.deltaTime * velocidad);

        if (biker.transform.position == siguiente.position)
        {
            siguiente = siguiente == fin ? inicio : fin;
        }
        if (biker.transform.position.x == inicio.position.x)
        {
            biker.transform.localScale = new Vector2(1, 1);
        } else
        if (biker.transform.position.x == fin.position.x)
        {
            biker.transform.localScale = new Vector2(-1, 1);
        }
	}
    void attack()
    {
        Collider2D tmp = Physics2D.OverlapCircle(this.attackpoint.position, 0.02f, this.whattoattack);
        if (tmp)
        {
            tmp.gameObject.SendMessage("hurt", 20);
        }

    }
}
