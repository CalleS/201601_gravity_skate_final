﻿using UnityEngine;
using System.Collections;

public class TomboAI : MonoBehaviour {
    public float walkspeed;
    public float maxfallspeed;
    public float jumpimpulse;

    public Transform groundcheckpoint;
    public LayerMask whatisground;

    public LayerMask whattoattack;
    public Transform attackpoint;
    public int damage;

    public Transform target;

    private Rigidbody2D body;
    private Vector2 movement;

    private float horinput;
    private bool iamintheground;
    private bool facingright;

    private Animator anim;

    private float time;
    private float timetoattack;
	// Use this for initialization
	void Start () {
        this.body = this.GetComponent<Rigidbody2D>();
        this.movement = new Vector2();
        

        this.anim = this.GetComponent<Animator>();
        this.facingright = true;

        this.time = 0;
        this.timetoattack = 1;

	}
    void OnCollisionEnter2D(Collision2D c)
    {
        iamintheground = c.gameObject.tag.Equals("Piso");
    }

    // Update is called once per frame
    void Update () {


        if (iamintheground)
        {
            body.velocity = new Vector2(body.velocity.x, jumpimpulse);
            iamintheground = false;         
        }

        if (this.transform.position.x < this.target.position.x) { this.horinput = 1; }
        else if(this.transform.position.x > this.target.position.x) { this.horinput = -1; }

        if ((this.horinput < 0) && (this.facingright))
        { this.facingright = false;
            this.flip();
        }else if ((this.horinput > 0) && (!this.facingright))
        {
            this.facingright = true;
            this.flip();
        }
        this.anim.SetFloat("HorSpeed", Mathf.Abs(this.body.velocity.x));
        this.anim.SetFloat("VertSpeed", Mathf.Abs(this.body.velocity.y));

        if(Physics2D.OverlapCircle(this.groundcheckpoint.position, 0.02f, this.whatisground))
        {
            this.iamintheground = true;
        }else { this.iamintheground = false; }
    }
    //==========================================
    void FixedUpdate()
    {
        this.movement = this.body.velocity;

        this.time += Time.deltaTime;

        if (this.time > this.timetoattack)
        {
            this.time = 0;
            this.Detect();
        }

        this.movement.x = horinput * walkspeed;
        if(!this.iamintheground)
        {
            if(this.movement.y < this.maxfallspeed)
            {
                this.movement.y = this.maxfallspeed;
            }
        }
        this.body.velocity = this.movement;
    }
    //==========================================
    void flip()
    {
        Vector3 scale = this.transform.localScale;
        scale.x *= (-1);
        this.transform.localScale = scale;
    }
    //=======================================================
    void Detect()
    {
        Collider2D tmp = Physics2D.OverlapCircle(this.attackpoint.position, 0.02f, this.whattoattack);
        if (tmp)
        {
            if (tmp.gameObject.CompareTag("Player"))
            {
                tmp.gameObject.SendMessage("hurt", this.damage);
            }

            
        }
    }
}
