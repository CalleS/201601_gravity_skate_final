﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour {
    public float hp;
    public Slider healthbar;
    private Vector2 pos_0;
   
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void hurt(float damage)
    {
        
        this.hp -= damage;
        if (this.healthbar)
        {
            this.healthbar.value = this.hp;
        }
        if (this.hp <= 0)
        {
            
            
            if(this.hp <= 0) { this.hp = 100;
                this.healthbar.value = this.hp;
                this.gameObject.SendMessage("OnGetKill");
                Destroy(this.gameObject);
            }
            }

    }

  

}
